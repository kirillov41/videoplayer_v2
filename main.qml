import QtQuick
import QtQuick.Window
import QtMultimedia
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Dialogs
import Qt.labs.platform 1.1

Window {
    width: 640
    height: 480
    visible: true
    minimumHeight: 300
    minimumWidth: 300
    title: qsTr("Видеопроигрыватель " + fileSelect.file)

    Timer{
        id: timer
        interval: 100
        running: false
        repeat: true
        onTriggered: {
            progressBar.value = video.position
        }
    }

    FileDialog {
        id: fileSelect
        visible: true
        onRejected: {
            Qt.quit()
        }
    }

    ColumnLayout{
        anchors.fill: parent
        spacing: 5
        Rectangle{
            color: "#000000"
            anchors.rightMargin: 5
            anchors.leftMargin: 5
            Layout.fillHeight: true
            Layout.fillWidth: true

                     Video {
                        id: video
                        source: fileSelect.file
                        anchors.fill: parent
                        focus: true
                        }
       }

        ProgressBar{
            id: progressBar
            Layout.rightMargin: 5
            Layout.leftMargin: 5
            Layout.minimumWidth: 30
            Layout.fillWidth: true


        }

        RowLayout{
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter


            spacing: 5
            Button{
                Layout.bottomMargin: 5
                Layout.minimumHeight: 50
                Layout.minimumWidth: 50
                Text {
                    text: qsTr("▶️")
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pointSize: parent.width / 2

                }

                onClicked: {
                    video.play()
                    timer.start()
                    progressBar.from = 0
                    progressBar.to = video.duration

                }
            }
            Button{
                Layout.bottomMargin: 5
                Layout.minimumHeight: 50
                Layout.minimumWidth: 50

                Text {
                    height: 30
                    text: qsTr("⏸️")
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pointSize: parent.width / 3

                }
                onClicked: {
                    timer.stop()
                    video.pause()
                }
            }
            Button{
                Layout.bottomMargin: 5
                Layout.minimumHeight: 50
                Layout.minimumWidth: 50
                Text {
                    height: 30
                    text: qsTr("⏹")
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pointSize: parent.width / 3

                }
                onClicked: {
                    timer.stop()
                    video.stop()
                    progressBar.value = 0
                }
            }
            Button{
                Layout.bottomMargin: 5
                Layout.minimumHeight: 50
                Layout.minimumWidth: 50
                Text {
                    height: 30
                    text: qsTr("⏪️")
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pointSize: parent.width / 3

                }
                onClicked: {
                    video.position-=1000
                    progressBar.value = video.position
                }
            }
            Button{
                Layout.bottomMargin: 5
                Layout.minimumHeight: 50
                Layout.minimumWidth: 50
                Text {
                    height: 30
                    text: qsTr("⏩️")
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pointSize: parent.width / 3

                }
                onClicked: {
                    video.position+=1000
                    progressBar.value = video.position
                }
            }
        }
    }

}

